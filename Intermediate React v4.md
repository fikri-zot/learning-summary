# Intermediate React v4

## Hooks in Depth

### useState

```jsx
import { useState } from "react";

const StateComponent = () => {
  const [isGreen, setIsGreen] = useState(true);

  return (
    <h1
      onClick={() => setIsGreen(!isGreen)}
      style={{ color: isGreen ? "limegreen" : "crimson" }}
    >
      useState Example
    </h1>
  );
};

export default StateComponent;
```

UseState is used to store a changing variable that will make the component rerender with the latest state.

### useEffect

```jsx
import { useState, useEffect } from "react";

const EffectComponent = () => {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    const timer = setTimeout(() => setTime(new Date()), 1000);
    return () => clearTimeout(timer);
  });

  return <h1>useEffect Example: {time.toLocaleTimeString()}</h1>;
};

export default EffectComponent;
```

In above useEffect, the time state will be updated every 100ms and will be called from the timeout callback. UseEffect is used on side-effect type action.

### useContext

```jsx
import { useState, useContext, createContext } from "react";

const UserContext = createContext([
  {
    firstName: "Bob",
    lastName: "Bobberson",
    suffix: 1,
    email: "bobbobberson@example.com"
  },
  (obj) => obj
]);

const LevelFive = () => {
  const [user, setUser] = useContext(UserContext);

  return (
    <div>
      <h5>{`${user.firstName} ${user.lastName} the ${user.suffix} born`}</h5>
      <button
        onClick={() => {
          setUser(Object.assign({}, user, { suffix: user.suffix + 1 }));
        }}
      >
        Increment
      </button>
    </div>
  );
};

const ContextComponent = () => {
  const userState = useState({
    firstName: "James",
    lastName: "Jameson",
    suffix: 1,
    email: "jamesjameson@example.com"
  });

  return (
    <UserContext.Provider value={userState}>
      <LevelFive />
    </UserContext.Provider>
  );
};

export default ContextComponent;
```

When the application components get bigger, managing react state will be quite handy especially when there is need to pass state from top level component to the child child level component. UseContext can be used to tackle this problem by wrapping all element using context provider that make the state be available from all across level component without the need to props chaining the state.

### useRef

```jsx
import { useState, useRef } from "react";

const RefComponent = () => {
  const [stateNumber, setStateNumber] = useState(0);
  const numRef = useRef(0);

  function incrementAndDelayLogging() {
    setStateNumber(stateNumber + 1);
    numRef.current++;
    setTimeout(
      () => alert(`state: ${stateNumber} | ref: ${numRef.current}`),
      1000
    );
  }

  return (
    <div>
      <h1>useRef Example</h1>
      <button onClick={incrementAndDelayLogging}>delay logging</button>
      <h4>state: {stateNumber}</h4>
      <h4>ref: {numRef.current}</h4>
    </div>
  );
};

export default RefComponent;
```

The element that is referred by useRef will give the consistent object. In the code above, the value of state and ref will be displayed different even though they are same. This happens due to state value that get delayed, the ref value will display the latest because it's not tied to the closure. The other case of useRef is to refer DOM nodes.

### useReducer

```jsx
import { useReducer } from "react";

const limitRGB = (num) => (num < 0 ? 0 : num > 255 ? 255 : num);

const step = 50;

const reducer = (state, action) => {
  switch (action.type) {
    case "INCREMENT_R":
      return Object.assign({}, state, { r: limitRGB(state.r + step) });
    case "DECREMENT_R":
      return Object.assign({}, state, { r: limitRGB(state.r - step) });
    default:
      return state;
  }
};

const ReducerComponent = () => {
  const [{ r, g, b }, dispatch] = useReducer(reducer, { r: 0, g: 0, b: 0 });

  return (
    <div>
      <h1 style={{ color: `rgb(${r}, ${g}, ${b})` }}>useReducer Example</h1>
      <div>
        <span>r</span>
        <button onClick={() => dispatch({ type: "INCREMENT_R" })}>➕</button>
        <button onClick={() => dispatch({ type: "DECREMENT_R" })}>➖</button>
      </div>
    </div>
  );
};

export default ReducerComponent;
```

When the state get more complex, will may consider to use useReducer to map every action based on how they update the state.

### useMemo

```jsx
import { useState, useMemo } from "react";

const fibonacci = (n) => {
  if (n <= 1) {
    return 1;
  }

  return fibonacci(n - 1) + fibonacci(n - 2);
};

const MemoComponent = () => {
  const [num, setNum] = useState(1);
  const [isGreen, setIsGreen] = useState(true);
  const fib = useMemo(() => fibonacci(num), [num]);

  return (
    <div>
      <h1
        onClick={() => setIsGreen(!isGreen)}
        style={{ color: isGreen ? "limegreen" : "crimson" }}
      >
        useMemo Example
      </h1>
      <h2>
        Fibonacci of {num} is {fib}
      </h2>
      <button onClick={() => setNum(num + 1)}>➕</button>
    </div>
  );
};

export default MemoComponent;
```

UseMemo is used due to performance optimization. Let's say we have a costly computational logic that need a long time to finish, instead of calculating the function (in the above code, fibonacci function), we can use useMemo to store the previous value and tell react to use it so that the function doesn't run every component rerender.

### useCallback

```jsx
import { useState, useEffect, useCallback, memo } from "react";

const ExpensiveComputationComponent = memo(({ compute, count }) => {
  return (
    <div>
      <h1>computed: {compute(count)}</h1>
      <h4>last re-render {new Date().toLocaleTimeString()}</h4>
    </div>
  );
});

const CallbackComponent = () => {
  const [time, setTime] = useState(new Date());
  const [count, setCount] = useState(1);
  useEffect(() => {
    const timer = setTimeout(() => setTime(new Date()), 1000);
    return () => clearTimeout(timer);
  });

  const fibonacci = (n) => {
    if (n <= 1) {
      return 1;
    }

    return fibonacci(n - 1) + fibonacci(n - 2);
  };

  return (
    <div>
      <h1>useCallback Example {time.toLocaleTimeString()}</h1>
      <button onClick={() => setCount(count + 1)}>
        current count: {count}
      </button>
      <ExpensiveComputationComponent
        compute={useCallback(fibonacci, [])}
        count={count}
      />
    </div>
  );
};

export default CallbackComponent;
```

UseCallback is same as useMemo. UseCallback is used to make sure that react using the same fibonacci function every rerender so that it can see the saved computational result that will use useMemo to retrieve the computed value.

### useLayoutEffect

```jsx
import { useState, useLayoutEffect, useRef } from "react";

const LayoutEffectComponent = () => {
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);
  const el = useRef();

  useLayoutEffect(() => {
    setWidth(el.current.clientWidth);
    setHeight(el.current.clientHeight);
  });

  return (
    <div>
      <h1>useLayoutEffect Example</h1>
      <h2>textarea width: {width}px</h2>
      <h2>textarea height: {height}px</h2>
      <textarea
        onClick={() => {
          setWidth(0);
        }}
        ref={el}
      />
    </div>
  );
};

export default LayoutEffectComponent;
```

UseLayoutEffect is same as useEffect, but it is used for layout purposes and act synchronously to component render. We can use useLayoutEffect to measure DOM nodes changes such as animation. For example when the text area in the above code is change, useLayoutEffect will rerun to change width and height state based on text area's.

### useImperativeHandle

```jsx
import { useState, useRef, useImperativeHandle, forwardRef } from "react";

const ElaborateInput = forwardRef(
  ({ hasError, placeholder, value, update }, ref) => {
    const inputRef = useRef();
    useImperativeHandle(ref, () => {
      return {
        focus() {
          inputRef.current.focus();
        }
      };
    });
    return (
      <input
        ref={inputRef}
        value={value}
        onChange={(e) => update(e.target.value)}
        placeholder={placeholder}
      />
    );
  }
);

const ImperativeHandleComponent = () => {
  const [state, setState] = useState("WA");
  const [error, setError] = useState("");
  const cityEl = useRef();
  const stateEl = useRef();

  function validate() {
    if (!/^[A-Z]{2}$/.test(state)) {
      setError("state");
      stateEl.current.focus();
      return;
    }

    setError("");
    alert("valid form!");
  }

  return (
    <div>
      <ElaborateInput
        hasError={error === "state"}
        placeholder={"State"}
        value={state}
        update={setState}
        ref={stateEl}
      />
      <button onClick={validate}>Validate Form</button>
    </div>
  );
};

export default ImperativeHandleComponent;
```

One of the use case of using useImperativeHandle is to make form focus. Let's say when user input the form content with field value, it will automatically focus the form with some css property to make it more visible.

### useDebugValue

```jsx
import { useState, useEffect, useDebugValue } from "react";

const useIsRaining = () => {
  const [isRaining, setIsRaining] = useState(false);

  useEffect(() => {
    setIsRaining(Math.random() > 0.5);
  }, []);

  useDebugValue(isRaining ? "Is Raining" : "Is Not Raining");

  return isRaining;
};

const DebugValueComponent = () => {
  const isRaining = useIsRaining();

  return (
    <div>
      <h1>useDebugValue Example</h1>
      <h2>Do you need a coat today? {isRaining ? "yes" : "maybe"}</h2>
    </div>
  );
};

export default DebugValueComponent;
```

UseDebugValue can be useful when we use custom hooks and want to debug it. It will display the state value to react dev tools and other debugging information related to the created custom hooks.

## TailwindCSS

In this section, tailwindCSS is used to make writing css more easy because we can write inline css in the jsx file. The content is basic tailwind, grid, breakpoint, positioning. For more references you can look into official tailwind docs.

## Code Splitting

When the application get bigger, we may consider to split our application by pages or other option so that the user doesn't need to load the unrequired resources (mainly javascript). 

```jsx
import { useState, StrictMode, lazy, Suspense } from "react";

// above const App =
const Details = lazy(() => import("./Details"));
const SearchParams = lazy(() => import("./SearchParams"));

// surround BrowserRouter
<Suspense fallback={<h1>loading route …</h1>}>[…]</Suspense>;
```

For the example, when we ran into detail page, we don't need to load searchParams. lazy function will tell react that the imported component will only be loaded when it's needed. The sunpense fallback will display some information while the lazy load the corresponding component.

## Server Side Rendering

Server side rendering is used so that user only need html for the first rendered page. It will give the performance advantage because the javascript will be loaded later on.

```jsx
import { hydrate } from "react-dom";
import { BrowserRouter, BrowserRouter as Router } from "react-router-dom";
import App from "./App";

hydrate(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root")
);
```

Hydrate is used instead of react render. 

```jsx
<script type="module" src="./ClientApp.js"></script>
```

In index.html, we add script tag that refer to ClientApp containing the required javascript file after the first html render.

```jsx
import express from "express";
import { renderToString } from "react-dom/server";
import { StaticRouter } from "react-router-dom/server";
import fs from "fs";
import App from "../src/App";

const PORT = process.env.PORT || 3000;

const html = fs.readFileSync("dist/frontend/index.html").toString();

const parts = html.split("not rendered");

const app = express();

app.use("/frontend", express.static("dist/frontend"));
app.use((req, res) => {
  const reactMarkup = (
    <StaticRouter location={req.url}>
      <App />
    </StaticRouter>
  );

  res.send(`${parts[0]}${renderToString(reactMarkup)}${parts[1]}`);
  res.end();
});

console.log(`listening on http://localhost:${PORT}`);
app.listen(PORT);
```

The above code is the example of using static rendering. Parcel will build the app into static html, the rest of functionality that parcell doesn't serve in static html will be covered by ClientApp. 

## Typescript

### Typescript and Eslint

Typescript is used to add the static checker into javascript file. This will make debugging easier because code will get checked immediately when it's typed in visual code. All .js extension will need to change to .tsx file. Some of the typescript refactor that need to be done:

- Add FunctionComponent to every react component

- Define the interface for react component props

- If there is any type in variable, it need to be assigned with some types

- The parameter and the return value of a function need to be explicitly typed when typescript can't automatically infer

## Redux

We might consider using redux when the useContext get more complex and hard to use, redux code is also testable and react have it's own redux debugging tools. But if there is none of the case above is appear and our state use case is fairly simple, we don't need to use it.

### Redux

```jsx
import { createStore } from "redux";
import reducer from "./reducers";

const store = createStore(
  reducer,
  typeof window === "object" &&
    typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined"
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : (f) => f
);

export default store;
```

The first thing to prepare for using redux is create a store that will store the reducer and initial state.

### Reducers

```jsx
import { combineReducers } from "redux";
import animal from "./animal";
import breed from "./breed";

export default combineReducers({
  animal,
  breed,
});  

export default function animal(state = "", action) {
  switch (action.type) {
    case "CHANGE_ANIMAL":
      return action.payload;
    default:
      return state;
  }
}  
export default function breed(state = "", action) {
  switch (action.type) {
    case "CHANGE_BREED":
      return action.payload;
    case "CHANGE_ANIMAL":
      return "";
    default:
      return state;
  }
}
```

After that reducers is created.

### Action Creators

```jsx
export default function changeBreed(location) {
  return { type: "CHANGE_BREED", payload: location };
}
```

Action creator is created so that we can call reducers from other component.

### Providers

```jsx
import { Provider } from "react-redux";
import store from "./store";

// wrap app with
<Provider store={store}>[…]</Provider>;
```

Provider is needed to wrap the entire app so that the redux state and action is available to all component.

```jsx
import { useSelector } from "react-redux";

const animal = useSelector((state) => state.animal);
const breed = useSelector((state) => state.breed);
```

To read the state, we need to import useSelector.

### Dispatching Actions

```jsx
import { useSelector, useDispatch } from "react-redux";
import changeLocation from "./actionCreators/changeLocation";

const dispatch = useDispatch();

<input
  id="location"
  value={location}
  placeholder="Location"
  onChange={(e) => dispatch(changeLocation(e.target.value))}
/>
```

The dispatcher will dispatch the action that created by action creator previously.

### Redux Dev Tools

[Redux DevTools - Chrome Web Store](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)

## Testing

### Testing React

React testing can be done by using Jest package. We can test from the react component and the integrated UI interactions.

### Basic React Testing

```jsx
test("displays a non-default thumbnail", async () => {
  const pet = render(
    <StaticRouter>
      <Pet images={["1.jpg", "2.jpg", "3.jpg"]} />
    </StaticRouter>
  );

  const petThumbnail = await pet.findByTestId("thumbnail");
  expect(petThumbnail.src).toContain("1.jpg");
});
```

The code above is the example to test Pet component that will be loaded and contain the 1.jpg image.

### Testing UI Interactions

```jsx
import { expect, test } from "@jest/globals";
import { render } from "@testing-library/react";
import Carousel from "../Carousel.js";

test("lets users click on thumbnails to make them the hero", async () => {
  const images = ["0.jpg", "1.jpg", "2.jpg", "3.jpg"];
  const carousel = render(<Carousel images={images} />);

  const hero = await carousel.findByTestId("hero");
  expect(hero.src).toContain(images[0]);

  for (let i = 0; i < images.length; i++) {
    const image = images[i];

    const thumb = await carousel.findByTestId(`thumbnail${i}`);
    thumb.click();

    expect(hero.src).toContain(image);
    expect(thumb.classList).toContain("active");
  }
});
```

The UI interactions can be done when the thumbnail is clicked that the component will have active class.

### Testing Custom Hooks

```jsx
import { expect, test } from "@jest/globals";
import { render } from "@testing-library/react";
import useBreedList from "../useBreedList.js";

function getBreedList(animal) {
  let list;

  function TestComponent() {
    list = useBreedList(animal);
    return null;
  }

  render(<TestComponent />);

  return list;
}

test("gives an empty list with no animal", async () => {
  const { result } = renderHook(() => useBreedList(""));

  const [breedList, status] = result.current;

  expect(breedList).toHaveLength(0);
  expect(status).toBe("unloaded");
});
```

For testing custom hook, we created a fake component that use the custom hooks.

### Mocks

```jsx
test("gives back breeds with an animal", async () => {
  const breeds = [
    "Havanese",
    "Bichon Frise",
    "Poodle",
    "Maltese",
    "Golden Retriever",
    "Labrador",
    "Husky",
  ];
  fetch.mockResponseOnce(
    JSON.stringify({
      animal: "dog",
      breeds,
    })
  );
  const { result, waitForNextUpdate } = renderHook(() => useBreedList("dog"));

  await waitForNextUpdate();

  const [breedList, status] = result.current;
  expect(status).toBe("loaded");
  expect(breedList).toEqual(breeds);
});
```

We can mocks the api calls so that it implement fake fetch and doesn't need to fetch the real api.

### Istanbul

The coverage code that get tested can be looked using https://istanbul.js.org/ .

### VSCode Extension

https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest is Jest VSCode extenstion to make working with Jest easier.
