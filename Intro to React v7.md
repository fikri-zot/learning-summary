# Frontend Masters: Intro to React v7

## Vanilla React

```jsx
<body>
  <div id="root">not rendered</div>
  <script src="https://unpkg.com/react@17.0.2/umd/react.development.js"></script>
  <script src="https://unpkg.com/react-dom@17.0.2/umd/react-dom.development.js"></script>
  <script src="./App.js"></script>
</body>
```

In vanilla react, we need to import react and react DOM to the script tag. 

```jsx
const Pet = (props) => {
  return React.createElement("div", {}, [
    React.createElement("h1", {}, props.name),
    React.createElement("h2", {}, props.animal),
    React.createElement("h2", {}, props.breed),
  ]);
};

const App = () => {
  return React.createElement("div", {}, [
    React.createElement("h1", {}, "Adopt Me!"),
    React.createElement(Pet, {
      name: "Luna",
      animal: "Dog",
      breed: "Havanese",
    }),
    React.createElement(Pet, {
      name: "Pepper",
      animal: "Bird",
      breed: "Cockatiel",
    }),
    React.createElement(Pet, { name: "Doink", animal: "Cat", breed: "Mix" }),
  ]);
};

ReactDOM.render(React.createElement(App), document.getElementById("root"));
```

After that, we can use React and ReactDOM. React is used to create an instance of element such as **div, h1, or h2**  and store it as a component into a function. ReactDOM is used to render the created components and pass it into html div element that have a **root** id.

## JS Tools

In this section there are some tools that can be used for the development of react app such as npm, prettier, eslint, git, parcel.

**Prettier** is used to format the semantical side of code such as coma, space, etc. **ESLint** is used to tell if there is some code that violate some defined rules such as unused variable. **Git** will track the changes of the code. **Parcel** will pack the code to the production ready build.

## Core React Concepts

### JSX

```jsx
const Pet = (props) => {
  return (
    <div>
      <h1>{props.name}</h1>
      <h2>{props.animal}</h2>
      <h2>{props.breed}</h2>
    </div>
  );
};

const App = () => {
  return (
    <div>
      <h1>Adopt Me!</h1>
      <Pet name="Luna" animal="dog" breed="Havanese" />
      <Pet name="Pepper" animal="bird" breed="Cockatiel" />
      <Pet name="Doink" animal="cat" breed="Mix" />
    </div>
  );
};
```

JSX makes react codes more readable. THe props can be passed into the inside of the component by declaring it in component parameter. The first letter of made up react component is capitalized to differentiate it from the basic html element.

### Hooks

```jsx
import { useState } from "react";

const [location, setLocation] = useState("");

<input
  id="location"
  value={location}
  placeholder="Location"
  onChange={(e) => setLocation(e.target.value)}
/>;
```

One of the most commonly used react hooks is useState. The code above use useState to store location value with the initial value of empty string. For every state changes, react will rerender the affected component with the new updated state value. Every key strokes on input element, the location will be updated by onChange attribute.  React hooks has some rules, for example we can't put hooks inside if statements or loops. 

### Effects

```jsx
import { useEffect, useState } from "react";
import Pet from "./Pet";

const [pets, setPets] = useState([]);

useEffect(() => {
  requestPets();
}, []);

async function requestPets() {
  const res = await fetch(
    `http://pets-v2.dev-apis.com/pets?animal=${animal}&location=${location}&breed=${breed}`
  );
  const json = await res.json();

  setPets(json.pets);
}

// inside the div element of jsx component
{
  pets.map((pet) => (
    <Pet name={pet.name} animal={pet.animal} breed={pet.breed} key={pet.id} />
  ));
}
```

React component must be free from side effect, it must be rendered immediately when it get accessed by the user. So when we want to fetch a data that takes some time to load, we need useEffect so that the times for data fetching doesn't affectthe react component to render for the first time. The UI for react component can be rendered early without the need to wait for the data fetching to finish. The [ ] is for data dependecies, react will look for the element inside the brackets and if the element is changed then the effect will re-run again.

### Custom Hooks

```jsx
import { useState, useEffect } from "react";

const localCache = {};

export default function useBreedList(animal) {
  const [breedList, setBreedList] = useState([]);
  const [status, setStatus] = useState("unloaded");

  useEffect(() => {
    if (!animal) {
      setBreedList([]);
    } else if (localCache[animal]) {
      setBreedList(localCache[animal]);
    } else {
      requestBreedList();
    }

    async function requestBreedList() {
      setBreedList([]);
      setStatus("loading");
      const res = await fetch(
        `http://pets-v2.dev-apis.com/breeds?animal=${animal}`
      );
      const json = await res.json();
      localCache[animal] = json.breeds || [];
      setBreedList(localCache[animal]);
      setStatus("loaded");
    }
  }, [animal]);

  return [breedList, status];
}
```

Sometimes we have a situation that require us to reuse the same thing over and over. For the above example, accessing the breed list need the useState and useEffect at the same time. For those kind of situation, we can use custom hooks. Custom hooks is made of react hooks. The use case of above code is using a cache to save fetched data to prevent over fetching. By using custom hooks, we encapsulate the hooks logic and the exposed value are only the beedList and the status that can be read in other component and made the codes looks cleaner.

### Handling User Input

```jsx
<form
  onSubmit={e => {
    e.preventDefault();
    requestPets();
  }}
>
```

The requestPets function above is async code that send the request when the form is submitted, the best way for handling user input is to put the handler function into onSubmit attribute of form.

### Component Composition

By breaking down the components to smaller components we can take the advantage of reusability and code organization. The code will be easier to maintain because one component is only responsible for one purpose. The common use case is to use map for the data that will be rendered many times.

```jsx
import Pet from "./Pet";

const Results = ({ pets }) => {
  return (
    <div className="search">
      {!pets.length ? (
        <h1>No Pets Found</h1>
      ) : (
        pets.map((pet) => {
          return (
            <Pet
              animal={pet.animal}
              key={pet.id}
              name={pet.name}
              breed={pet.breed}
              images={pet.images}
              location={`${pet.city}, ${pet.state}`}
              id={pet.id}
            />
          );
        })
      )}
    </div>
  );
};

export default Results;  

// at the top level component
import Results from "./Results";

<Results pets={pets} />;
```

We pass the pets data to Results component that will render the Pet component if there is any pet in the data and will display No Pets Found if there is no pet data.

### React Dev Tools

We mainly use development server when debugging react code because it provides additional logs utility, for example the key rule for component only show when it's on the development mode. We can achieve this when using Parcel. For the production ready, the app bundle will have smaller size than the development mode. React have a useful chrome extensions for the developers to view and edit react state and component directly.

```jsx
import { StrictMode } from "react";

const App = () => {
  return (
    <StrictMode>
      <div>
        <h1>Adopt Me!</h1>
        <SearchParams />
      </div>
    </StrictMode>
  );
};
```

StrictMode is available to use when we want to highlight the potential problems in our application.

## React Capabilites

### React Router

```jsx
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Details from "./Details";

<BrowserRouter>
  <h1>Adopt Me!</h1>
  <Routes>
    <Route path="/details/:id" element={<Details />} />
    <Route path="/" element={<SearchParams />} />
  </Routes>
</BrowserRouter>;
```

React router package is used for the application router.  The BrowserRouter element is placed in the outer of the rest react element such as routes and route. To use dynamic route, we can put for example :id in the path of route component. 

```jsx
import { useParams } from "react-router-dom";

const Details = () => {
  const { id } = useParams();
  return <h2>{id}</h2>;
};

export default Details;
```

The id from dynamic route can be read by using useParams. React router dom also provides Link component to provide better user experience when switching between pages.

### Class Components

```jsx
import { Component } from "react";
import { useParams } from "react-router-dom";

class Details extends Component {
  constructor() {
    super();
    this.state = { loading: true };
  }

  async componentDidMount() {
    const res = await fetch(
      `http://pets-v2.dev-apis.com/pets?id=${this.props.params.id}`
    );
    const json = await res.json();
    this.setState(Object.assign({ loading: false }, json.pets[0]));
  }

  render() {
    if (this.state.loading) {
      return <h2>loading … </h2>;
    }

    const { animal, breed, city, state, description, name } = this.state;

    return (
      <div className="details">
        <div>
          <h1>{name}</h1>
          <h2>{`${animal} — ${breed} — ${city}, ${state}`}</h2>
          <button>Adopt {name}</button>
          <p>{description}</p>
        </div>
      </div>
    );
  }
}

const WrappedDetails = () => {
  const params = useParams();
  return <Details params={params} />;
};

export default WrappedDetails;
```

The old way of writing react component is by writing class based components. I don't dive too dep into this section. The other section that discussed in class components is class properties and managing state in class components. 

## Special Case React Tools

### Error Boundaries

```jsx
import { Component } from "react";
import { Link } from "react-router-dom";

class ErrorBoundary extends Component {
  state = { hasError: false };
  static getDerivedStateFromError() {
    return { hasError: true };
  }
  componentDidCatch(error, info) {
    console.error("ErrorBoundary caught an error", error, info);
  }
  render() {
    if (this.state.hasError) {
      return (
        <h2>
          There was an error with this listing. <Link to="/">Click here</Link>{" "}
          to back to the home page or wait five seconds.
        </h2>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
```

Error boundary can be used to catch an error so that the application doesn't break and it will display the error message. It use the class components.

### Portals and Refs

```jsx
import React, { useEffect, useRef } from "react";
import { createPortal } from "react-dom";

const Modal = ({ children }) => {
  const elRef = useRef(null);
  if (!elRef.current) {
    elRef.current = document.createElement("div");
  }

  useEffect(() => {
    const modalRoot = document.getElementById("modal");
    modalRoot.appendChild(elRef.current);
    return () => modalRoot.removeChild(elRef.current);
  }, []);

  return createPortal(<div>{children}</div>, elRef.current);
};

export default Modal;
```

On of the use case of using portal and refs together is when we want to show modals. We create a html element above root element with some id that will targeted by portal so that the modal won't mess up the DOM structure of the current app.
