# Build a Modern User Interface with Chakra UI

## Installing Chakra UI

Run these following commands:

```
npm i @chakra-ui/react @emotion/react @emotion/styled framer-motion

```

Wrap the app component with ChakraProvider

```jsx
import { ChakraProvider } from '@chakra-ui/react'

const App = () => {
    return (
    <ChakraProvider>
        <TheRestOfOtherComponents/>
    </ChakraProvider>
    )
}
```

## Building Form with SimpleGrid, FormControl, and Input Component

ChakraUI provides Container, Flex, and VStack Component as the basic component. The container will wrap the component to some width. Flex will give the div with flex style and VStack is used when we want so stack some components that have same space in between.

```jsx
import {
  FormControl,
  FormLabel,
  Input,
  VStack,
  Heading,
  Text,
  SimpleGrid,
  GridItem,
  Select,
  Checkbox,
  Button,
} from '@chakra-ui/react';

const Details = () => {
  return (
    <VStack w="full" h="full" p={10} spacing={10} alignItems="flex-start">
      <VStack spacing={3} alignItems="flex-start">
        <Heading size="2xl">Your details</Heading>
        <Text>If you already have an account, click here to log in.</Text>
      </VStack>
      <SimpleGrid columns={2} columnGap={3} rowGap={6} w="full">
        <GridItem colSpan={1}>
          <FormControl>
            <FormLabel>First Name</FormLabel>
            <Input placeholder="John" />
          </FormControl>
        </GridItem>
        <GridItem colSpan={1}>
          <FormControl>
            <FormLabel>Last Name</FormLabel>
            <Input placeholder="Doe" />
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <FormControl>
            <FormLabel>Address</FormLabel>
            <Input placeholder="Blvd. Broken Dreams 21" />
          </FormControl>
        </GridItem>
        <GridItem colSpan={1}>
          <FormControl>
            <FormLabel>City</FormLabel>
            <Input placeholder="San Francisco" />
          </FormControl>
        </GridItem>
        <GridItem colSpan={1}>
          <FormControl>
            <FormLabel>Country</FormLabel>
            <Select>
              <option value="usa">United States of America</option>
              <option value="uae">United Arab Emirates</option>
              <option value="nmk">North Macedonia</option>
              <option value="de">Germany</option>
            </Select>
          </FormControl>
        </GridItem>
        <GridItem colSpan={2}>
          <Checkbox defaultChecked>Ship to billing address.</Checkbox>
        </GridItem>
        <GridItem colSpan={2}>
          <Button size="lg" w="full">
            Place Order
          </Button>
        </GridItem>
      </SimpleGrid>
    </VStack>
  );
};

export default Details;
```

Above is the example of writing a component VStack, Grid, and FormControl.

## Create a Dark Mode Switcher

Import the useColorMode hooks from '@chakra-ui/react' and use it inside the component.

```jsx
const { toggleColorMode } = useColorMode();
const bgColor = useColorModeValue('gray.50', 'whiteAlpha.50')
const secondaryTextColor = useColorModeValue('gray.600','gray.400')

<Button onClick={toggleColorMode}>Change color mode</Button>
```

The bgColor can be used to specify the two color modes and secondaryTextColor is used to specify text color modes with the different color from bgColor.

## Implement Responsive Design

ChakraUI use mobile first approach for the responsiveness. We can specify the responsiveness in many different way.

- Using array
  
  ```jsx
  <Flex py={[0, 10, 20]}>
      <Details/>
  </Flex>
  ```

- Specify the berakpoint
  
  ```jsx
  <Flex direction={{ base: 'column-reverse', md: 'row'}}>
      <Details/>
  </Flex>
  ```

- UseBreakpointValue
  
  ```jsx
  // inside the component that have column span
  const colSpan = useBreakpointValue({base: 2, md: 1})
  ```

## Define Custom Colors and Fonts

```jsx
// index.ts inside src/theme
import { extendTheme, theme as base } from '@chakra-ui/react';

const theme = extendTheme({
  colors: {
    brand: {
      50: '#f5fee5',
      100: '#e1fbb2',
      200: '#cdf781',
      300: '#b8ee56',
      400: '#a2e032',
      500: '#8ac919',
      600: '#71ab09',
      700: '#578602',
      800: '#3c5e00',
      900: '#203300',
    },
  },
  fonts: {
    heading: `Montserrat, ${base.fonts?.heading}`,
    body: `Inter, ${base.fonts?.body}`,
  },
});

export default theme;  

  
// inside App component
import theme from '../src/theme'

<ChakraProvider theme={theme}>
    <Rest.../>
<ChakraProvider/>
```

Custom colors and fonts can be specified by creating a new file and use extendTheme. For the fonts we need to specify the fallback value just in case the custom fonts can't be loaded by the user. The theme is imported into App component and used as a attribute in ChakraProvider component.

## Use Theme Extensions

We can apply some theme extensions into chakra ui component by specifying it in theme file that we already created. Add the code below inside src/theme/index.ts to customize the checkbox, input, and select color & variant.

```jsx
  withDefaultColorScheme({
    colorScheme: 'brand',
    components: ['Checkbox'],
  }),
  withDefaultVariant({
    variant: 'filled',
    components: ['Input', 'Select'],
  })
```

## Override the Built-in Component

```jsx
components: {
      Input: { ...inputSelectStyles },
      Select: { ...inputSelectStyles },
      Checkbox: {
        baseStyle: {
          control: {
            borderRadius: 'none',
            _focus: {
              ring: 2,
              ringColor: 'brand.500',
            },
          },
        },
      },
    },
```

We can override the built in component by specify the replaced custom element for the component inside the theme file.

## Create Custom Variants

```jsx
const inputSelectStyles = {
  variants: {
    filled: {
      field: {
        _focus: {
          borderColor: 'brand.500',
        },
      },
    },
  },
  sizes: {
    md: {
      field: {
        borderRadius: 'none',
      },
    },
  },
};

const brandRing = {
  _focus: {
    ring: 2,
    ringColor: 'brand.500',
  },
};  

// inside extendTheme constant
components: {
      Button: {
        variants: {
          primary: (props) => ({
            rounded: 'none',
            ...brandRing,
            color: mode('white', 'gray.800')(props),
            backgroundColor: mode('brand.500', 'brand.200')(props),

            _hover: {
              backgroundColor: mode('brand.600', 'brand.300')(props),
            },

            _active: {
              backgroundColor: mode('brand.700', 'brand.400')(props),
            },
          }),
        },
      },
      Input: { ...inputSelectStyles },
      Select: { ...inputSelectStyles },
      Checkbox: {
        baseStyle: {
          control: {
            borderRadius: 'none',
            ...brandRing,
          },
        },
      },
    },
```

The custom variants can be created and used in some components, in the above code we specify the custom border color for input and select component.
